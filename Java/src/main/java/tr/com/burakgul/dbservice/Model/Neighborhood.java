package com.basarsoft.dbservice.Model;


import javax.persistence.*;

@Entity
@Table(name="neighborhood")
public class Neighborhood {


    @Id
    @GeneratedValue
    private int neigborhoodid;

    @Column(name="districtid")
    private int districtid;

    @Column(name="neighborhoodname")
    private String neighborhoodname;

    public int getNeigborhoodid() {
        return neigborhoodid;
    }

    public void setNeigborhoodid(int neigborhoodid) {
        this.neigborhoodid = neigborhoodid;
    }

    public int getDistrictid() {
        return districtid;
    }

    public void setDistrictid(int districtid) {
        this.districtid = districtid;
    }

    public String getNeighborhoodname() {
        return neighborhoodname;
    }

    public void setNeighborhoodname(String neighborhoodname) {
        this.neighborhoodname = neighborhoodname;
    }
}
