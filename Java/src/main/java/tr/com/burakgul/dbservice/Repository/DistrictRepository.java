package com.basarsoft.dbservice.Repository;

import com.basarsoft.dbservice.Model.District;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DistrictRepository extends JpaRepository<District,Integer> {
}
