package com.basarsoft.dbservice.Model;

import org.locationtech.jts.geom.Geometry;

import javax.persistence.*;

@Entity
@Table(name="city")
public class City {

    @Id
    @GeneratedValue
    private int cityid;
    @Column(name="citygeom")
    private Geometry geoloc;
    @Column(name="cityname")
    private String cityname;

    public int getCityid() {
        return cityid;
    }

    public void setCityid(int cityid) {
        this.cityid = cityid;
    }

    public Geometry getGeoloc() {
        return geoloc;
    }

    public void setGeoloc(Geometry geoloc) {
        this.geoloc = geoloc;
    }

    public String getCityname() {
        return cityname;
    }

    public void setCityname(String cityname) {
        this.cityname = cityname;
    }
}
