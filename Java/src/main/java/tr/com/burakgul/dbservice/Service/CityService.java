package com.basarsoft.dbservice.Service;


import com.basarsoft.dbservice.Exception.RecordNotFoundException;
import com.basarsoft.dbservice.Model.City;
import com.basarsoft.dbservice.Repository.CityRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service

public class CityService {

    CityRepository repository;

    public CityService(CityRepository repository){

        this.repository = repository;

    }

    public List<City> getAllCity(){

        List<City> cityList = repository.findAll();

        if(cityList.size() > 0){
            return cityList;
        }else {
            return new ArrayList<City>();
        }

    }


    public City getCityById(Integer id) throws RecordNotFoundException {

        Optional<City> city = repository.findById(id);

        if(city.isPresent()){
            return city.get();
        }else{
            throw new RecordNotFoundException("Girilen id ile eşleşen şehir bulunamadı.");
        }
    }

    public City createOrUpdateCity(City entity) throws RecordNotFoundException{

        Optional<City> city = repository.findById(entity.getCityid());

        if(city.isPresent()){

            City newEntity = city.get();
            newEntity.setCityname(entity.getCityname());
            newEntity = repository.save(entity);
            return newEntity;

        }else{
            entity = repository.save(entity);
            return entity;
        }
    }

    public void deleteCityById (Integer id) throws RecordNotFoundException{

        Optional<City> city = repository.findById(id);

        if(city.isPresent()){
            repository.deleteById(id);
        }else{
            throw new RecordNotFoundException("Verilen id ile eşleşen şehir bulunamadı.");
        }
    }

}
