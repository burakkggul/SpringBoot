package com.basarsoft.dbservice.Model;

import javax.persistence.*;

@Entity
@Table(name="district")
public class District {

    @Id
    @GeneratedValue
    private int districtid;



    @Column(name="cityid")
    private int cityid;

    @Column(name="districtname")
    private String districtname;

    public int getDistrictid() {
        return districtid;
    }

    public void setDistrictid(int districtid) {
        this.districtid = districtid;
    }

    public int getCityid() {
        return cityid;
    }

    public void setCityid(int cityid) {
        this.cityid = cityid;
    }

    public String getDistrictname() {
        return districtname;
    }

    public void setDistrictname(String districtname) {
        this.districtname = districtname;
    }
}
