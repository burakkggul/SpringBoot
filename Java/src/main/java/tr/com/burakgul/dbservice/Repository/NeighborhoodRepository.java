package com.basarsoft.dbservice.Repository;

import com.basarsoft.dbservice.Model.Neighborhood;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NeighborhoodRepository extends JpaRepository<Neighborhood,Integer> {
}
